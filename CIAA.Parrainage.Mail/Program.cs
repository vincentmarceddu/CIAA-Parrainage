﻿using CIAA.Parrainage.Core;
using CIAA.Parrainage.Core.services;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Injection;

namespace CIAA.Parrainage.Mail
{
    class Program
    {
        static void Main(string[] args)
        {
            var unityContainer = UnityConfig.UnitySetup();
            var logger = LogManager.GetLogger(typeof(Program));
            var mailService = new MailService(logger,
                ConfigurationManager.AppSettings["BaseLink"],
                ConfigurationManager.AppSettings["Year"],
                ConfigurationManager.AppSettings["Credentials:MailAddress"],
                ConfigurationManager.AppSettings["Credentials:Password"],
                ConfigurationManager.AppSettings["Port"],
                ConfigurationManager.AppSettings["Host"],
                ConfigurationManager.AppSettings["PollAnswersLink"],
                Properties.MailResources.SendLinkTemplate);

            var sponsorService = unityContainer.Resolve<SponsorService>();

            var sponsors = sponsorService.GetAllSponsors().GetAwaiter().GetResult();

            logger.Info("1 - Envoyer le lien de choix.");
            logger.Info("2 - Envoyer les résultats finaux.");

            bool choiceCorrect = false;
            while(!choiceCorrect)
                switch (Console.ReadLine())
                {
                    case "1":
                        choiceCorrect = true;
                        sponsors.ForEach(currentSponsor => mailService.SendUIDLinkToSponsor(currentSponsor));
                        break;
                    case "2":
                        choiceCorrect = true;
                        //sponsors.ForEach(currentSponsor => mailService.SendUIDLinkToSponsor(currentSponsor));
                        break;
                    default:
                        logger.Info("Choix inexistant.");
                        break;
                }

            Console.Read();
        }


    }
}
