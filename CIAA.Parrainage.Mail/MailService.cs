﻿using CIAA.Parrainage.Core.models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CIAA.Parrainage.Mail
{
    public class MailService
    {
        private readonly ILog _Logger;
        private readonly string _BaseLinkToSend;
        private readonly string _Year;
        private readonly string _CredentialsMailAddress;
        private readonly string _SendLinkTemplate;
        private readonly SmtpClient _Client;

        public MailService(ILog logger, string baseLinkToSend, string year, string credentialsMailAddress, string credentialsPassword, string port, string host,
            string pollAnswersLink, string SendLinkTemplate)
        {
            this._Logger = logger;
            this._BaseLinkToSend = baseLinkToSend;
            this._Year = year;
            this._CredentialsMailAddress = credentialsMailAddress;
            this._SendLinkTemplate = SendLinkTemplate;

            _SendLinkTemplate = _SendLinkTemplate.Replace("#{Students_Answer_List}#", pollAnswersLink);
            this._Client = new SmtpClient
            {
                Port = int.Parse(port),
                Host = host,
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(_CredentialsMailAddress, credentialsPassword)
            };
        }

        public void SendUIDLinkToSponsor(Sponsor sponsor)
        {
            if (sponsor.Student == null)
            {
                if (sponsor.MailAddress == "vincent.marceddu@gmail.com")
                {
                    string queryString = $"?year={_Year}&UID={sponsor.UID}";
                    string messageToSend = _SendLinkTemplate;

                    messageToSend = messageToSend.Replace("#{Sponsor_Identity}#", $"{sponsor.FirstName} {sponsor.LastName}");
                    messageToSend = messageToSend.Replace("#{Choice_Link}#", _BaseLinkToSend + queryString);

                    MailMessage mm = new MailMessage(_CredentialsMailAddress, sponsor.MailAddress, "Parrainage", messageToSend)
                    {
                        BodyEncoding = UTF8Encoding.UTF8
                    };
                    _Client.Send(mm);
                }
            }
        }
    }
}
