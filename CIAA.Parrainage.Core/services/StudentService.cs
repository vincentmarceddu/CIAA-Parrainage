﻿using CIAA.Parrainage.Core.models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CIAA.Parrainage.Core.services
{
    public class StudentService
    {
        private readonly ILog _Logger;

        public string ServiceUsername { get; set; }
        public string ServicePassword { get; set; }
        public string GetStudentsServiceEndpoint { get; set; }
        public string Year { get; set; }


        public StudentService(ILog logger)
        {
            _Logger = logger;
        }

        public async Task<List<Student>> GetAllStudents()
        {
            _Logger.Info("Début de la récupération de tous les filleul(e)s.");

            var serviceURI = GetStudentsServiceEndpoint;

            serviceURI += $"?year={Year}";

            using (HttpClient client = new HttpClient())
            {
                var byteArray = Encoding.ASCII.GetBytes($"{ServiceUsername}:{ServicePassword}");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                try
                {
                    HttpResponseMessage response = await client.GetAsync(serviceURI);
                    HttpContent content = response.Content;

                    var jsonResult = await content.ReadAsStringAsync();
                    var studentsFromJson = JsonConvert.DeserializeObject<List<Student>>(jsonResult);

                    _Logger.Info($"Récupération réussie, {studentsFromJson.Count} récupérés.");

                    return studentsFromJson;
                }
                catch (Exception ex)
                {
                    _Logger.Error("Erreur lors de la récupération.", ex);
                    throw ex;
                }
            }
        }
    }
}