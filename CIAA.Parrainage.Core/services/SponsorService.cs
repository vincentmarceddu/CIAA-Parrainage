﻿using CIAA.Parrainage.Core.models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CIAA.Parrainage.Core.services
{
    public class SponsorService
    {
        private readonly ILog _Logger;

        public string ServiceUsername { get; set; }
        public string ServicePassword { get; set; }
        public string GetSponsorsServiceEndpoint { get; set; }
        public string PostSponsorUIDServiceEndpoint { get; set; }
        public string Year { get; set; }


        public SponsorService(ILog logger)
        {
            _Logger = logger;
        }

        public async void SetSponsorUID(Sponsor sponsor, Guid guid)
        {
            _Logger.Info($"Set du GUID pour {sponsor}");

            sponsor.UID = guid.ToString();

            var parameters = new Dictionary<string, string> { { "sponsorFirstName", sponsor.FirstName }, { "sponsorLastName", sponsor.LastName },
                { "UID", sponsor.UID}, { "year", Year } };

            using (HttpClient client = new HttpClient())
            {
                var byteArray = Encoding.ASCII.GetBytes($"{ServiceUsername}:{ServicePassword}");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                try
                {
                    HttpResponseMessage response = await client.PostAsync(PostSponsorUIDServiceEndpoint, new FormUrlEncodedContent(parameters));
                    HttpContent content = response.Content;

                    var resp = await content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception($"{response.StatusCode} : {resp}");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error("Erreur lors du set de GUID : ", ex);
                }
            }

        }

        public async Task<List<Sponsor>> GetAllSponsors()
        {
            _Logger.Info("Début de la récupération de tous les parrains / marraines.");

            var serviceURI = GetSponsorsServiceEndpoint;

            serviceURI += $"?year={Year}";

            using (HttpClient client = new HttpClient())
            {
                var byteArray = Encoding.ASCII.GetBytes($"{ServiceUsername}:{ServicePassword}");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                try
                {
                    HttpResponseMessage response = await client.GetAsync(serviceURI);
                    HttpContent content = response.Content;

                    var jsonResult = await content.ReadAsStringAsync();
                    var sponsorsFromJson = JsonConvert.DeserializeObject<List<Sponsor>>(jsonResult);

                    _Logger.Info($"Récupération réussie, {sponsorsFromJson.Count} récupérés.");

                    return sponsorsFromJson;
                }
                catch (Exception ex)
                {
                    _Logger.Error("Erreur lors de la récupération.", ex);
                    throw ex;
                }
            }
        }
    }
}
