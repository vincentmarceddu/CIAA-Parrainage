﻿using CIAA.Parrainage.Core.models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CIAA.Parrainage.Core.services
{
    public class MailService
    {
        private readonly ILog _Logger;
        private readonly string _BaseLinkToSend;
        private readonly string _Year;
        private readonly string _CredentialsMailAddress;
        private readonly SmtpClient _Client;

        public MailService(ILog logger, string baseLinkToSend, string year, string credentialsMailAddress, string credentialsPassword, string port, string host)
        {
            this._Logger = logger;
            this._BaseLinkToSend = baseLinkToSend;
            this._Year = year;
            this._CredentialsMailAddress = credentialsMailAddress;
            this._Client = new SmtpClient
            {
                Port = int.Parse(port),
                Host = host,
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(_CredentialsMailAddress, host)
            };
        }

        public void SendUIDLinkToSponsor(Sponsor sponsor)
        {
            if(sponsor.MailAddress == "vincent.marceddu@gmail.com")
            {
                string queryString = $"?year={_Year}&UID={sponsor.UID}";
                string messageToSend = $"Bonjour {sponsor.FirstName} {sponsor.LastName}, voici le lien vers ton accès : {_BaseLinkToSend + queryString}"; 
                MailMessage mm = new MailMessage(_CredentialsMailAddress, sponsor.MailAddress, "Parrainnage", "test")
                {
                    BodyEncoding = UTF8Encoding.UTF8
                };
                _Client.Send(mm);
            }
        }
    }
}
