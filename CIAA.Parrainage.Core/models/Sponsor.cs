﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIAA.Parrainage.Core.models
{
    public class Sponsor
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MailAddress { get; set; }
        public string UID { get; set; }
        public List<Student> Choice { get; set; }
        public Student Student { get; set; }
        public override string ToString() => $"{FirstName} {LastName}";
    }

}

