﻿using CIAA.Parrainage.Core.services;
using log4net;
using log4net.Config;
using System.Configuration;
using Unity;
using Unity.Injection;
using Unity.log4net;

namespace CIAA.Parrainage.Core
{
    public class UnityConfig
    {

        public static UnityContainer UnitySetup()
        {
            XmlConfigurator.Configure();

            var container = new UnityContainer();
            container.AddNewExtension<Log4NetExtension>();
            container.RegisterType<SponsorService>(
                new InjectionProperty("ServiceUsername", ConfigurationManager.AppSettings["Service:Username"]),
                new InjectionProperty("ServicePassword", ConfigurationManager.AppSettings["Service:Password"]),
                new InjectionProperty("GetSponsorsServiceEndpoint", ConfigurationManager.AppSettings["ServiceGetSponsors:Endpoint"]),
                new InjectionProperty("PostSponsorUIDServiceEndpoint", ConfigurationManager.AppSettings["ServicePostSponsorUID:Endpoint"]),
                new InjectionProperty("Year", ConfigurationManager.AppSettings["Year"])
             );

            container.RegisterType<StudentService>(
                new InjectionProperty("ServiceUsername", ConfigurationManager.AppSettings["Service:Username"]),
                new InjectionProperty("ServicePassword", ConfigurationManager.AppSettings["Service:Password"]),
                new InjectionProperty("GetStudentsServiceEndpoint", ConfigurationManager.AppSettings["ServiceGetStudents:Endpoint"]),
                new InjectionProperty("Year", ConfigurationManager.AppSettings["Year"])
            );

            return container;
        }
    }
}