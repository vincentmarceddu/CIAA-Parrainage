﻿using CIAA.Parrainage.Core;
using CIAA.Parrainage.Core.services;
using log4net.Config;
using System;
using System.Configuration;
using Unity;
using Unity.Injection;

namespace CIAA.Parrainage.Generation
{
    class Program
    {
        static void Main(string[] args)
        {
            var unityContainer = UnityConfig.UnitySetup();

            var sponsorService = unityContainer.Resolve<SponsorService>();

            var sponsors = sponsorService.GetAllSponsors().GetAwaiter().GetResult();
            sponsors.FindAll(p => String.IsNullOrEmpty(p.UID)).ForEach(currentSponsor => sponsorService.SetSponsorUID(currentSponsor, Guid.NewGuid()));

            Console.Read();
        }
    }
}
