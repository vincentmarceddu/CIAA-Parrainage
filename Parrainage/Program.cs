﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Parrainage
{
    class Program
    {
        async static void test()
        {
            var serviceURI = ConfigurationManager.AppSettings["Service:Url"];
            var serviceUsername = ConfigurationManager.AppSettings["Service:Username"];
            var servicePassword = ConfigurationManager.AppSettings["Service:Password"];
            var year = ConfigurationManager.AppSettings["Year"];

            serviceURI += $"?year={year}";

            // ... Use HttpClient.            
            HttpClient client = new HttpClient();

            var byteArray = Encoding.ASCII.GetBytes($"{serviceUsername}:{servicePassword}");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            HttpResponseMessage response = await client.GetAsync(serviceURI);
            HttpContent content = response.Content;

            // ... Check Status Code                                
            Console.WriteLine("Response StatusCode: " + (int)response.StatusCode);

            // ... Read the string.
            string result = await content.ReadAsStringAsync();

            Console.WriteLine(result);

            JsonConvert
        }

        static void Main(string[] args)
        {
            test();
            Console.Read();
        }
    }
}
