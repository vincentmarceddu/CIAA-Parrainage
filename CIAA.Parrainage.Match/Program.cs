﻿using CIAA.Parrainage.Core;
using CIAA.Parrainage.Core.models;
using CIAA.Parrainage.Core.services;
using log4net;
using System;
using System.Configuration;
using Unity;

namespace CIAA.Parrainage.Match
{
    class Program
    {
        static void Main(string[] args)
        {
            var unityContainer = UnityConfig.UnitySetup();

            var logger = LogManager.GetLogger(typeof(Program));

            var sponsorService = unityContainer.Resolve<SponsorService>();
            var sponsors = sponsorService.GetAllSponsors().GetAwaiter().GetResult();
            var sponsorsWithoutStudentCount = sponsors.FindAll(sponsor => sponsor.Student == null).Count;

            var studentsService = unityContainer.Resolve<StudentService>();
            var students = studentsService.GetAllStudents().GetAwaiter().GetResult();
            var studentsWithoutSponsorCount = students.Count - (sponsors.Count - sponsorsWithoutStudentCount);

            if (sponsorsWithoutStudentCount == studentsWithoutSponsorCount)
            {
                //Override choices if fake data
                if (bool.Parse(ConfigurationManager.AppSettings["FakeData"]))
                {
                    logger.Info("Génération aléatoire de choix pour les parrains / marraines.");
                    sponsors.ForEach(sponsor => sponsor.GenerateFakeChoice(students));
                }

                var matchJob = new MatchJob(logger, sponsors, students);
                matchJob.Initialize();
                matchJob.BeginMatches();
            }
            else
            {
                logger.Error("Le nombre de parrains / marraines sans filleul(e) n'est pas égal au nombre de filleul(e) sans parrain / marraine");
            }


            Console.ReadLine();
        }
    }
}
