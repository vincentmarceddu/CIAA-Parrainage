﻿using System.Collections.Generic;
using CIAA.Parrainage.Core.models;
using log4net;
using System.Linq;
using System;

namespace CIAA.Parrainage.Match
{

    public class Match
    {
        public Student StudentInstance { get; set; }
        public Sponsor FinalSponsor { get; set; }
        public List<KeyValuePair<Sponsor, int>> Sponsors { get; set; }

        public override string ToString() => $"{StudentInstance} : {Sponsors.Count} prétendants";
    }

    public class MatchJob
    {
        private readonly ILog _Logger;
        private readonly List<Sponsor> _Sponsors;
        private readonly List<Student> _Students;
        private List<Match> _Matches;

        public MatchJob(ILog logger, List<Sponsor> sponsors, List<Student> students)
        {
            _Logger = logger;
            _Sponsors = sponsors;
            _Students = students;
            _Matches = new List<Match>();
        }

        public void Initialize()
        {
            _Logger.Info("Suppresion des paires déjà formées.");
            _Sponsors.FindAll(sponsor => sponsor.Student != null).ForEach(sponsorWithStudent =>
            {
                _Logger.Info($"\t{sponsorWithStudent} et {sponsorWithStudent.Student}");
                _Sponsors.Remove(sponsorWithStudent);
                _Students.RemoveAll(student => (student.FirstName == sponsorWithStudent.Student.FirstName && student.LastName == sponsorWithStudent.Student.LastName));
            });

            _Logger.Info("Initialisation des couples.");
            _Students.ForEach(student =>
            {
                _Matches.Add(new Match()
                {
                    StudentInstance = student,
                    FinalSponsor = null,
                    Sponsors = _Sponsors.Select(sponsor => new KeyValuePair<Sponsor, int>(sponsor, sponsor.Choice.GetStudentOrder(student))).Where(kvPair => kvPair.Value != -1).ToList()
                });
            });
        }

        public void BeginMatches()
        {
            var remainingPendingMatches = _Matches.FindAll(match => match.FinalSponsor == null);

            _Logger.Info("Début du processus.");

            var cyclesNumber = 0;

            while (cyclesNumber++ != 2)
            {
                _Logger.Info($"Cycle n°{cyclesNumber}");

                for (int choicePriorityIndex = 0; choicePriorityIndex < 3; choicePriorityIndex++)
                {
                    _Logger.Info($"\tPasse (Seul en choix #{choicePriorityIndex})");
                    while (!((remainingPendingMatches = AloneInChoicePriorityPass(remainingPendingMatches, choicePriorityIndex)) == (remainingPendingMatches))) ;
                    _Logger.Info($"\tFin de la première passe, {remainingPendingMatches.Count} couples restants.{Environment.NewLine}");

                    _Logger.Info($"\tDeuxième passe (Aléatoire parmi tous les choix #{choicePriorityIndex})");
                    while (!((remainingPendingMatches = RandomOnChoicePriorityPass(remainingPendingMatches, choicePriorityIndex)) == (remainingPendingMatches))) ;
                    _Logger.Info($"\tFin de la deuxième passe, {remainingPendingMatches.Count} couples restants.{Environment.NewLine}");
                }
            }

            //_Logger.Info($"Passe intermédiaire (Pour chaque Student, si plusieurs Sponsor restants : choix aléatoire du Sponsor");
            //remainingPendingMatches = IntermediatePass(remainingPendingMatches);
            //_Logger.Info($"Fin de la passe intermédiaire, {remainingPendingMatches.Count} couples restants.{Environment.NewLine}");

            //_Logger.Info($"Passe Finale (Plus aucune correspondance Student-Sponsor : Répartition aléatoire)");
            //remainingPendingMatches = FinalPass(remainingPendingMatches);
            //_Logger.Info($"Fin de la passe finale, {remainingPendingMatches.Count} couples restants.{Environment.NewLine}");

        }

        private List<Match> AloneInChoicePriorityPass(List<Match> remainingMatches, int choicePriority)
        {
            remainingMatches.ForEach(match =>
            {
                var firstChoiceSponsor = match.Sponsors.FindAll(kvSponsor => kvSponsor.Value == 0);
                if (firstChoiceSponsor.Count == 1)
                    TrySetFinalSponsor(remainingMatches, match, firstChoiceSponsor[0].Key);
            });

            return remainingMatches.FindAll(match => match.FinalSponsor == null);
        }

        private List<Match> RandomOnChoicePriorityPass(List<Match> remainingMatches, int choicePriority)
        {
            var rand = new Random(Guid.NewGuid().GetHashCode());

            remainingMatches.ForEach(match =>
            {
                var duelSponsorChoice = match.Sponsors.FindAll(sponsorOfMatch => sponsorOfMatch.Value == choicePriority);
                    TrySetFinalSponsor(remainingMatches, match, duelSponsorChoice.PickOneRandom(rand).Key);
            });

            return remainingMatches.FindAll(match => match.FinalSponsor == null);
        }

        //private List<Match> FirstPass(List<Match> remainingMatches)
        //{
        //    remainingMatches.ForEach(match =>
        //    {
        //        if (match.Sponsors.Count == 1)
        //            TrySetFinalSponsor(_Matches, match, match.Sponsors[0].Key);
        //    });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        //private List<Match> SecondPass(List<Match> remainingMatches)
        //{
        //    remainingMatches.ForEach(match =>
        //    {
        //        var firstChoiceSponsor = match.Sponsors.FindAll(kvSponsor => kvSponsor.Value == 0);
        //        if (firstChoiceSponsor.Count == 1)
        //            TrySetFinalSponsor(remainingMatches, match, firstChoiceSponsor[0].Key);
        //    });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        //private List<Match> ThirdPass(List<Match> remainingMatches)
        //{
        //    remainingMatches.ForEach(match =>
        //    {
        //        var secondChoiceSponsor = match.Sponsors.FindAll(kvSponsor => kvSponsor.Value == 1);
        //        if (secondChoiceSponsor.Count == 1)
        //            TrySetFinalSponsor(remainingMatches, match, secondChoiceSponsor[0].Key);
        //    });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        //private List<Match> FourthPass(List<Match> remainingMatches)
        //{
        //    var rand = new Random(Guid.NewGuid().GetHashCode());

        //    for (int choicePriority = 0; choicePriority < 3; choicePriority++)
        //        remainingMatches.ForEach(match =>
        //        {
        //            var duelSponsorChoice = match.Sponsors.FindAll(sponsorOfMatch => sponsorOfMatch.Value == choicePriority);
        //            if (duelSponsorChoice.Count == 2)
        //            {
        //                TrySetFinalSponsor(remainingMatches, match, duelSponsorChoice.PickOneRandom(rand).Key);
        //            }
        //        });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        //private List<Match> IntermediatePass(List<Match> remainingMatches)
        //{
        //    var rand = new Random(Guid.NewGuid().GetHashCode());

        //    remainingMatches.FindAll(match => match.Sponsors.Count != 0).ForEach(match =>
        //    {
        //        TrySetFinalSponsor(remainingMatches, match, match.Sponsors.PickOneRandom(rand).Key);
        //    });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        //private List<Match> FinalPass(List<Match> remainingMatches)
        //{
        //    var rand = new Random(Guid.NewGuid().GetHashCode());

        //    remainingMatches.ForEach(match =>
        //    {
        //        TrySetFinalSponsor(remainingMatches, match, _Sponsors.FindAll(sponsor => sponsor.Student == null).PickOneRandom(rand));
        //    });

        //    return remainingMatches.FindAll(match => match.FinalSponsor == null);
        //}

        private bool TrySetFinalSponsor(List<Match> matches, Match match, Sponsor sponsor)
        {
            if (sponsor.Student == null)
            {
                sponsor.Student = match.StudentInstance;
                match.FinalSponsor = sponsor;
                match.Sponsors.Clear();
                _Logger.Info($"\t\tAssociation {match.StudentInstance} - {match.FinalSponsor}.");

                matches.ForEach(matchFromMatches => matchFromMatches.Sponsors.RemoveAll(sponsorFromMatch => sponsorFromMatch.Key == sponsor));
                return true;
            }

            return false;
        }

    }
}