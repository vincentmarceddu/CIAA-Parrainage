﻿using CIAA.Parrainage.Core.models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CIAA.Parrainage.Match
{
    static class SponsorExtentHelper
    {
        public static void GenerateFakeChoice(this Sponsor sponsor, List<Student> students)
        {
            sponsor.Choice = new List<Student>();
            var rand = new Random(Guid.NewGuid().GetHashCode());
            while(sponsor.Choice.Count != 3)
            {
                Student studentPicked = default(Student);
                while (sponsor.Choice.Contains(studentPicked = students.PickOneRandom(rand))) ;
                sponsor.Choice.Add(studentPicked);
            }
        }

        public static T PickOneRandom<T>(this List<T> collection, Random rand)
        {
            return collection[rand.Next(collection.Count)];
        }

        public static int GetStudentOrder(this List<Student> students, Student studentToFind)
        {
            var order = -1;

            if(students != null)
                students.ForEach(student =>
                {
                    if (student.LastName == studentToFind.LastName && student.FirstName == studentToFind.FirstName)
                        order = students.IndexOf(student);
                });

            return order;
        }
    }
}